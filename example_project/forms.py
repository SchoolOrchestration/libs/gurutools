from django import forms
from django.shortcuts import get_object_or_404
from gurutools.fields import CommaSeparatedCharField
from example_app.models import Todo

class SetTodoStatusForm(forms.Form):

    status = forms.CharField(max_length=10)

    def save(self, request, pk=None):
        status = self.cleaned_data.get('status')
        todo = get_object_or_404(Todo, id=pk)
        todo.status = status
        todo.save()
        return ({}, todo)

class BulkSetTodoStatusForm(forms.Form):

    todos = CommaSeparatedCharField()
    status = forms.CharField(max_length=10)

    def save(self, request, pk=None):
        todo_ids = self.cleaned_data.get('todos')
        status = self.cleaned_data.get('status')
        todos = Todo.objects.filter(id__in=todo_ids)
        todos.update(**{
            "status": status
        })
        return ({}, todos)