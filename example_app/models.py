from django.db import models

TODO_STATUSES = [
    ('N', 'Todo'),
    ('P', 'In Progress'),
    ('D', 'Done'),
]

class Todo(models.Model):
    owner = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    status = models.CharField(max_length=30, choices=TODO_STATUSES)
    size = models.PositiveIntegerField(default=1)
    due = models.DateField()
    due_time = models.DateTimeField()

