from gurutools.actions.util import STATUS, ACTION_TYPE
TODO_ACTIONS = {
    "resource": "appointment",
    "basepath": "/todos/",
    "actions": {
        # instance:
        "setstatus": {
            "title": "Change a todo's status",
            "status": STATUS.PROD.value,
            "type": ACTION_TYPE.INSTANCE.value,
            "version": 1,
            "description": """Set a new status on a todo""",
            "form": "example_project.forms.SetTodoStatusForm",
            "serializer": "example_project.urls.TodoSerializer",
            "example_payload": {
                'todo': 1,
                'status': 'P'
            }
        },
        # bulk
        "setstatuses": {
            "title": "Change a collection of todo's statuses",
            "status": STATUS.PROD.value,
            "type": ACTION_TYPE.BULK.value,
            "version": 1,
            "description": """Update the status on a number of Todos""",
            "form": "example_project.forms.BulkSetTodoStatusForm",
            "serializer": "example_project.urls.TodoSerializer",
            "example_payload": {
                'todos': '1,2,3',
                'status': 'P'
            }
        },
    }
}
