## Mailer

```python
send.apply_async(
    args,
    queue='unibox-2',
    serializer='json'
)
```

## Pusher

### Pubnub:

```python
from gurutools.bindings.pusher import delegate_push
channel = "foo"
content = {}
delegate_push(
    method_name = "push_to_pubnub",
    args = (channel, content),
    version = "1"
)
```

### Stream.io

```python
from gurutools.bindings.pusher import activity_update
context = {"spaces": [9], "practitioners": [1,125], "owner": 1}
actor_id = 1
activity_update(
    actor_id,
    'appointmentview', # feed
    'toggle_calendar', # verb
    context,
    'toggling the calendar' # custom message
)

```