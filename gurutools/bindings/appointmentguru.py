from celery import shared_task

from .settings import APPOINTMENTGURU_QUEUE


@shared_task(name="schedule.tasks.invoice_paid")
def invoice_paid(invoice):
    pass


@shared_task(name="schedule.tasks.invoice_sent")
def invoice_sent(invoice):
    """
    from invoice.enrichment_serializers import InvoiceEnrichSerializer
    invoice_serialized = invoice._get_payload(serializer=InvoiceEnrichSerializer)
    data = {"payload": invoice_serialized}
    args = (data)
    send.apply_async(args, queue='appointmentguru',serializer='json')
    """
    pass
