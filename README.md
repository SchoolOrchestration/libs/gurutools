# GuruTools

[![PyPI version](https://badge.fury.io/py/gurutools.svg)](https://badge.fury.io/py/gurutools)

## Getting started

### Tests:

```
docker-compose run --rm web python manage.py test
```

### Run example app

```
docker-compose up
```

### Run integration tests:

```
python3 

>> from gurutools.integration import plays
```
